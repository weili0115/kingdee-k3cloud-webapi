package com.link.kingdee;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.util.StringUtils;

import com.link.kingdee.k3cloud.K3CloudTemplate;
import com.link.kingdee.k3cloud.domain.Authentication;
import com.link.kingdee.k3cloud.domain.BillQuery;

import junit.framework.TestCase;

@Slf4j
public class AppTest extends TestCase {
	
	@Test
	public void testBillQuery() {
		Authentication auth = new Authentication("${账套id}", "${用户名}", "${密码}", 2052);
		K3CloudTemplate template = new K3CloudTemplate("${url}", auth);
		List<BillQuery> params = new ArrayList<>();
		String[] fieldKeys = {
			      "FBOOKID","FNumber","FName"
			    };
	    String filter = "";
	    BillQuery query = new BillQuery("BD_AccountBook", StringUtils.arrayToDelimitedString(fieldKeys, ","), filter, 0, 0);
	    query.setTopRowCount(10000);
		List<?> dataList = template.executeBillQuery(query, AccountBook.class);
		log.info("AccountBookList = {}", dataList);
		assertNotNull(dataList);
	}

	/**
	 * 自定义账簿类用于接收返回数据
	 * <p>使用jackson @JsonProperty来对应金蝶返回字段</p>
	 */
	@Data
	public static class AccountBook {

		@JsonProperty("FBOOKID")
		private Integer bookId;

		@JsonProperty("FNumber")
		private Integer number;

		@JsonProperty("FName")
		private String name;

	}
	
}
