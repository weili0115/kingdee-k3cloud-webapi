package com.link.kingdee.k3cloud;

import java.util.List;

import com.link.kingdee.k3cloud.domain.Authentication;
import com.link.kingdee.k3cloud.domain.RequestService;

/**
 * k3cloud接口服务
 * @author dong.tang
 *
 * @param <P> 参数
 * @param <R> 返回值
 */
public interface ApiService<P, R> {
	
	String getEndPoint();
	
	RequestService getService();
	
	Authentication getAuth();
	
	R execute(List<P> params);

}
