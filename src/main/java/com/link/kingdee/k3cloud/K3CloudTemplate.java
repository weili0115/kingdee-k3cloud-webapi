package com.link.kingdee.k3cloud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;

import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.link.kingdee.k3cloud.domain.Authentication;
import com.link.kingdee.k3cloud.domain.BillQuery;
import com.link.kingdee.k3cloud.service.BillQueryService;

/**
 * 简化k3cloud操作
 * 
 * @author dong.tang
 *
 */
public class K3CloudTemplate implements K3CloudOperations {

	private final BillQueryService billQueryService;
	
	private final ObjectMapper objectMapper;

	public K3CloudTemplate(String endPoint, Authentication auth) {
		billQueryService = new BillQueryService();
		billQueryService.setEndPoint(endPoint);
		billQueryService.setAuth(auth);
		objectMapper = new ObjectMapper();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T executeBillQuery(List<BillQuery> params, Class<T> type) {
		String result = billQueryService.execute(params);
		ObjectMapper m = new ObjectMapper();
		try {
			return m.readValue(result, type);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return (T) result;
	}
	
	@SuppressWarnings("unchecked")
    @Override
    public <T> List<T> executeBillQuery(BillQuery billQuery, Class<T> type) {
        Assert.notNull(billQuery, "the billQuery param must be not null");
        List<BillQuery> params = new ArrayList<>(1);
        params.add(billQuery);
        List<List<?>> billList = executeBillQuery(params, List.class);
        // mapper key-value
        String[] fields = billQuery.getFields();
        List<T> dataList = billList.parallelStream().map(list -> {
            Map<String, Object> bill = new HashMap<>();
            for(int i = 0; i < fields.length; i++) {
                bill.put(fields[i], list.get(i));
            }
            return objectMapper.convertValue(bill, type);
        }).collect(toList());
        return dataList;
    }

}
