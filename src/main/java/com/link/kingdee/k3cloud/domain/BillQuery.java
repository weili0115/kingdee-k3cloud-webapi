package com.link.kingdee.k3cloud.domain;

import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillQuery {
	
	private String formId;
	
	private String fieldKeys;
	
	private String filterString;
	
	private String orderString;
	
	// 最多允许查询的数量
	private int topRowCount = 0;
	
	// 分页取数开始行索引
	private int startRow = 0; 
	
	// 无限制
	private int limit = 0;
	
	public BillQuery(String formId, String fieldKeys, String filterString, int startRow, int limit) {
		this(formId, fieldKeys, filterString, null, 0, startRow, limit);
	}
	
	public String[] getFields() {
        return StringUtils.isEmpty(fieldKeys) ? new String[] {} : fieldKeys.split(",");
    }
	
}
