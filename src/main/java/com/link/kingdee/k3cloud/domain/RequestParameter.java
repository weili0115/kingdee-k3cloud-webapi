package com.link.kingdee.k3cloud.domain;

import java.util.List;
import java.util.UUID;

import cn.hutool.json.JSONUtil;
import lombok.Data;

@Data
public class RequestParameter<P> {
	
	private String format = "1";
	
	private String nonce;
	
	private String version = "1.0";
	
	private String timestamp = String.valueOf(System.currentTimeMillis());
	
	private String useragent = "ApiClient";
	
	private String rid = String.valueOf(UUID.randomUUID().toString().hashCode());
	
	private List<P> parameters;
	
	@Override
	public String toString() {
		return JSONUtil.toJsonStr(this);
	}
	
}
