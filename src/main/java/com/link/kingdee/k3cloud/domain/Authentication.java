package com.link.kingdee.k3cloud.domain;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Authentication {

	// 帐套id
	private String actId;

	private String username;

	private String password;

	// 语言标识
	private int lcid = 2052;
	
	public List<?> toReqParamList() {
		return Arrays.asList(actId, username, password, lcid);
	}
	
}
