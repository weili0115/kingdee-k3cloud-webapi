package com.link.kingdee.k3cloud;

import java.util.List;

import com.link.kingdee.k3cloud.domain.BillQuery;

public interface K3CloudOperations {
	
	<T> T executeBillQuery(List<BillQuery> params, Class<T> type);
	
	<T> List<T> executeBillQuery(BillQuery billQuery, Class<T> type);
	
}
